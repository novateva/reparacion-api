+--------+----------+-----------------------------------------+------------------+----------------------------------------------------------------------------+--------------+
| Domain | Method   | URI                                     | Name             | Action                                                                     | Middleware   |
+--------+----------+-----------------------------------------+------------------+----------------------------------------------------------------------------+--------------+
|        | GET|HEAD | /                                       |                  | Closure                                                                    | web          |
|        | DELETE   | api/delete/{user_id}                    |                  | App\Http\Controllers\API\UserController@delete                             | api          |
|        | POST     | api/details                             |                  | App\Http\Controllers\API\UserController@details                            | api,auth:api |
|        | POST     | api/login                               |                  | App\Http\Controllers\API\UserController@login                              | api          |
|        | POST     | api/logout                              |                  | App\Http\Controllers\API\UserController@logout                             | api,auth:api |
|        | POST     | api/register                            |                  | App\Http\Controllers\API\UserController@register                           | api          |
|        | PUT      | api/update                              |                  | App\Http\Controllers\API\UserController@update                             | api,auth:api |
|        | GET|HEAD | api/users                               |                  | App\Http\Controllers\API\UserController@users                              | api          |
|        | GET|HEAD | home                                    | home             | App\Http\Controllers\HomeController@index                                  | web,auth     |
|        | POST     | login                                   |                  | App\Http\Controllers\Auth\LoginController@login                            | web,guest    |
|        | GET|HEAD | login                                   | login            | App\Http\Controllers\Auth\LoginController@showLoginForm                    | web,guest    |
|        | POST     | logout                                  | logout           | App\Http\Controllers\Auth\LoginController@logout                           | web          |
|        | GET|HEAD | oauth/authorize                         |                  | \Laravel\Passport\Http\Controllers\AuthorizationController@authorize       | web,auth     |
|        | DELETE   | oauth/authorize                         |                  | \Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny        | web,auth     |
|        | POST     | oauth/authorize                         |                  | \Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve  | web,auth     |
|        | POST     | oauth/clients                           |                  | \Laravel\Passport\Http\Controllers\ClientController@store                  | web,auth     |
|        | GET|HEAD | oauth/clients                           |                  | \Laravel\Passport\Http\Controllers\ClientController@forUser                | web,auth     |
|        | DELETE   | oauth/clients/{client_id}               |                  | \Laravel\Passport\Http\Controllers\ClientController@destroy                | web,auth     |
|        | PUT      | oauth/clients/{client_id}               |                  | \Laravel\Passport\Http\Controllers\ClientController@update                 | web,auth     |
|        | POST     | oauth/personal-access-tokens            |                  | \Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store     | web,auth     |
|        | GET|HEAD | oauth/personal-access-tokens            |                  | \Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser   | web,auth     |
|        | DELETE   | oauth/personal-access-tokens/{token_id} |                  | \Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy   | web,auth     |
|        | GET|HEAD | oauth/scopes                            |                  | \Laravel\Passport\Http\Controllers\ScopeController@all                     | web,auth     |
|        | POST     | oauth/token                             |                  | \Laravel\Passport\Http\Controllers\AccessTokenController@issueToken        | throttle     |
|        | POST     | oauth/token/refresh                     |                  | \Laravel\Passport\Http\Controllers\TransientTokenController@refresh        | web,auth     |
|        | GET|HEAD | oauth/tokens                            |                  | \Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser | web,auth     |
|        | DELETE   | oauth/tokens/{token_id}                 |                  | \Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy | web,auth     |
|        | POST     | password/email                          | password.email   | App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail      | web,guest    |
|        | GET|HEAD | password/reset                          | password.request | App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm     | web,guest    |
|        | POST     | password/reset                          |                  | App\Http\Controllers\Auth\ResetPasswordController@reset                    | web,guest    |
|        | GET|HEAD | password/reset/{token}                  | password.reset   | App\Http\Controllers\Auth\ResetPasswordController@showResetForm            | web,guest    |
|        | GET|HEAD | register                                | register         | App\Http\Controllers\Auth\RegisterController@showRegistrationForm          | web,guest    |
|        | POST     | register                                |                  | App\Http\Controllers\Auth\RegisterController@register                      | web,guest    |
+--------+----------+-----------------------------------------+------------------+----------------------------------------------------------------------------+--------------+
