
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <div class="jumbotron text-center">
	  <h1 class="display-3"> Hola {{$name}}</h1>
	  <p class="lead"><strong>Por favor confirma tu correo electrónico.</strong> Para ello simplemente debes hacer click en el siguiente enlace:</p>
	  <hr>
	  <a href="{{ url('/api/users/verify/' . $confirmation_code) }}">
        Click para confirmar tu email
    </a>
	 </div>
</body>
</html>
