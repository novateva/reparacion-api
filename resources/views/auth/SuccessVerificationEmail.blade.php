
<!DOCTYPE html>
<html style="height: 100%;" lang="es">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body style="height: 100%;">
    <div style="height: 100%;" class="jumbotron text-center">
	  <h1 class="display-3"> Hola {{$name}}</h1>
	  <p class="lead"><strong>Gracias por verificar tu correo!</strong> Accede a la aplicacion de data lab recovery para disfrutrar de todos nuestros servicios.</p>
	  <hr>
	 </div>
</body>
</html>