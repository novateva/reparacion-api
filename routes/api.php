<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('forgot', 'AuthController@forgot');
    Route::post('reset', 'AuthController@doReset')
        ->name('auth.reset');
});

Route::group([
    'prefix' => 'users'
], function () {
    Route::group(['middleware' => 'jwt'], function () {
        Route::put('update', 'UserController@update');
        Route::delete('delete', 'UserController@delete');
        Route::get('devices', 'UserController@devices');
    });
    Route::get('/', 'UserController@users');
    Route::get('{user_id}', 'UserController@user');
    Route::post('register', 'UserController@register');
    Route::get('/verify/{code}', 'UserController@verify');
    Route::get('/send_verfication_email/{email}', 'UserController@sendVerificationEmail');
});



Route::group([
    'prefix' => 'devices'
], function () {
    Route::get('/', 'DeviceController@devices');
    Route::get('{device_id}', 'DeviceController@device');
    Route::post('create', 'DeviceController@create');
    Route::group(['middleware' => 'jwt'], function () {
        Route::put('update', 'DeviceController@update');
        Route::delete('delete', 'DeviceController@delete');
    });
});

Route::group([
    'prefix' => 'notif'
], function () {
    Route::post('notifyall', 'NotificationController@notifyAll');
    Route::post('notify', 'NotificationController@notifyUser');
    Route::group(['middleware' => 'jwt'], function () {
        Route::get('all', 'NotificationController@allNotifications');
        Route::get('unread', 'NotificationController@unreadNotifications');
        Route::post('markasread', 'NotificationController@markAsRead');
    });
});
