<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
});

use Illuminate\Http\Request;

Auth::routes();

Route::get('auth/resetform', function (Request $request) {
    return view('auth.passwords.reset')->with('token', $request->token);
})->name('reset');
