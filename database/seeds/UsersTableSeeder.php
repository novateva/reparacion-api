<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::create([
            'email' => 'arturo.beltran@non.schneider-electric.com',
            'password' => Hash::make('secret'),
            'name' => 'Arturo',
            'phone' => '+131251235123',
            'confirmation_code' => "administrador"
        ]);
    }
}
