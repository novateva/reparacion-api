<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        App::setLocale('es');

        if(config('app.env') === 'production') {
            \URL::forceScheme('https');
        }

        Validator::extend('notification', function ($attribute, $value, $parameters, $validator) {
            return in_array($value, ['Report', 'Order']);
        });
        Validator::replacer('notification', function($message, $attribute, $rule, $parameters) {
            return str_replace($message, "Invalid notification type", $message);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
