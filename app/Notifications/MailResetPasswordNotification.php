<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


class MailResetPasswordNotification extends ResetPassword
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('vendor.notifications.email')
            ->from('recuperacion@gmail.com')
            ->subject('Notificación de reseteo de contraseña')
            ->greeting('Hola!')
            ->line('Estás recibiendo este correo porque recibimos una solicitud de cambio de contraseña para tu cuenta')
            ->action('Resetear Contraseña', url(config('app.url').route('reset', ['token' => $this->token], false)))
            ->line('Si no solicitaste un cambio de contraseña puedes ignorar este correo.'); 
    }

}
