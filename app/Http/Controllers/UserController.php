<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Validation\Rule;
use \Mail;
use App\Http\Requests\DeleteUser;
use App\Http\Requests\WriteUser;

class UserController extends Controller 
{
    public $successStatus = 200;

    public function register(WriteUser $request) 
    { 
        $input = $request->only(['name', 'email', 'password', 'phone', 'notifiable']); 
        $input['password'] = bcrypt($input['password']);
        $input['notifiable'] = $request->notifiable == '0' ? false : true;
        $input['confirmation_code'] = uniqid();
        $user = User::create($input);
        $user->save();
        $this->sendVerificationEmail($user->email);
        return response()->json(['user' => $user], $this->successStatus); 
    }

    public function users() 
    { 
        $users = User::all(); 
        return response()->json(['users' => $users], $this->successStatus); 
    } 

    public function user($user_id) 
    { 
        $user = User::findOrFail($user_id);
        return response()->json(['user' => $user], $this->successStatus); 
    } 

    public function delete(DeleteUser $request)
    {
        $user = User::findOrFail($request->id);
        $user->delete();
        return response()->json(['user' => $user], $this->successStatus);
    }

    public function update(WriteUser $request)
    {
        $user = Auth::user();

        $user->fill([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'notifiable' => $request->notifiable == '0' ? false : true
        ]);

        $user->save();

        return response()->json(['user' => $user], $this->successStatus); 
    }

    public function devices()
    {
        return Auth::user()->devices;
    }

    public function verify($code)
    {
        $user = User::where('confirmation_code', $code)->first();

        if (! $user)
            return view('auth/FailVerificationEmail');

        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();

        return view('auth/SuccessVerificationEmail', ['name'=> $user->name]);
    }

    public function sendVerificationEmail($email) {
        $user = User::where('email',$email)->first();
        if (!$user) {
            return ['status' => false];
        }
        Mail::send('auth.emailVerification', $user->toArray(), function($message) use ($user) {
            $message->to($user['email'], $user['name'])->subject('Correo de verificación de data recovery lab.');
        });
        return ['status' => true];
    }
}