<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetNotifications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use App\User;
use OneSignal;
use App\Http\Requests\NotifyAll;
use App\Http\Requests\NotifyUser;

class NotificationController extends Controller
{
    private function _notif_type(string $type)
    {
      return 'App\Notifications' . '\\' . $type;
    }
   
    public function notifyAll(NotifyAll $request)
    {
      $notif_type = $this->_notif_type($request->type);
      OneSignal::sendNotificationToAll(
        $request->body,
        $request->url,
        $request->data,
        null,
        null,
        $request->subject
      );
      Notification::send(User::where('notifiable', 1)->get(), new $notif_type(request()->all()));     
      return response()->json(['message' => "Notifications send"]);
    }

    public function notifyUser(NotifyUser $request)
    {
      $notif_type = $this->_notif_type($request->type);
      $user = User::findOrFail($request->id);
      if (!$user->notifiable)
        return response()->json(['message' => 'User has notifications turned off']);
      $ids = array();
      $active_devices = 0;
      foreach ($user->devices as $key => $device) {
        if ($device->valid) {
          array_push($ids, $device->token);
          $active_devices++;
        }
      }

      if ($active_devices) {
        OneSignal::sendNotificationToUser(
          $request->body,
          $ids,
          $request->url,
          $request->data,
          null,
          null,
          $request->subject
        );
        $user->notify(new $notif_type(request()->all()));
        return response()->json(['response' => "Notification send"]);
      }
     
      return response()->json(['response' => "User doesn't has any active devices"]);
    }

    public function unreadNotifications(GetNotifications $request)
    {
      $notifications = Auth::user()->unreadNotifications();

      if ($request->type) {
        $notif_type = $this->_notif_type($request->type);
        $notifications->where('type', $notif_type);
      }
      
      return response()->json([
        'notifications' => $notifications->get()
      ]);
    }

    public function allNotifications(GetNotifications $request)
    {
      $notifications = Auth::user()->notifications();

      if ($request->type) {
        $notif_type = $this->_notif_type($request->type);
        $notifications->where('type', $notif_type);
      }
      
      return response()->json([
        'notifications' => $notifications->get()
      ]);
    }

    public function markAsRead(GetNotifications $request)
    {
      $user = Auth::user();

      $notif_type = null;

      if ($request->type) 
        $notif_type = $this->_notif_type($request->type);
    
      foreach ($user->unreadNotifications as $notification) {
        if ($notif_type) {
          if ($notification->type === $notif_type) 
            $notification->markAsRead();
        }
        else 
          $notification->markAsRead();
      }

      return response()->json(['message' => 'success']);
    }

}
