<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Device; 
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Validation\Rule;

use App\Http\Requests\DeleteDevice;
use App\Http\Requests\RegisterDevice;
use App\Http\Requests\UpdateDevice;

class DeviceController extends Controller 
{
    public $successStatus = 200;

    public function create(RegisterDevice $request) 
    { 
        $input = $request->all();      
        $device = Device::create($input); 
        $device->save();
        return response()->json(['device' => $device], $this->successStatus); 
    }

    public function devices() 
    { 
        return response()->json(['devices' => Device::all()], $this->successStatus); 
    } 

    public function device($device_id) 
    { 
        $device = Device::findOrFail($device_id);
        return response()->json(['device' => $device], $this->successStatus); 
    } 

    public function delete(DeleteDevice $request)
    {
        $device = Device::findOrFail($request->id);
        $device->delete();
        return response()->json(['device' => $device], $this->successStatus);
    }

    public function update(UpdateDevice $request)
    {
        $device = Device::find($request->id);
        $device->user_id = $request->user_id;
        $device->fill([
            'type' => $request->type,
            'token' => $request->token,
            'valid' => $request->valid
        ]);

        $device->save();

        return response()->json(['device' => $device], $this->successStatus); 
    }

}